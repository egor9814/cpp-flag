//
// Created by egor9814 on 30 Jul 2021.
//

#ifndef FLAG_FLAG_HPP
#define FLAG_FLAG_HPP

#include <memory>
#include <string>
#include <functional>
#include <map>
#include <vector>
#include <iosfwd>

namespace flag {

	using BoolVar = std::shared_ptr<bool>;
	using StringVar = std::shared_ptr<std::string>;
	using IntVar = std::shared_ptr<int>;

	using UsageFunction = std::function<void()>;

	/// @class "IValue" is the interface to the dynamic value stored in a flag.
	struct IValue {
		virtual ~IValue() = default;

		[[nodiscard]] virtual std::string toString() const = 0;

		virtual void set(const std::string &s) = 0;

		[[nodiscard]] virtual bool isBool() const {
			return false;
		}

		[[nodiscard]] virtual bool isString() const {
			return false;
		}
	};
	using Value = std::shared_ptr<IValue>;

	/// A @class "Flag" represents the state of a flag.
	struct Flag {
		std::string name;         /// name as it appears on command line
		std::string usage;        /// help message
		Value value;              /// value as set
		std::string defaultValue; /// default value (as text); for usage message

		Flag(std::string name, std::string usage, Value value, std::string defaultValue);
	};

	/// A @class "FlagSet" represents a set of defined flags. The zero value of a FlagSet
	/// has no name and has ContinueOnError error handling.
	class FlagSet {
		UsageFunction usage;
		std::string name;
		bool parsed{false};

		using FlagPtr = std::shared_ptr<Flag>;
		std::map<std::string, FlagPtr> actual, formal;

		std::vector<std::string> args; /// arguments after flags

	public:
		enum class ErrorHandling {
			ContinueOnError = 0, /// Return a descriptive error.
			ExitOnError,         /// Call ::exit(2) or for -h/-help ::exit(0).
			ThrowOnError,        /// Throw exception with a descriptive error.
		};

		using Writer = std::shared_ptr<std::basic_ostream<char>>;

	private:
		ErrorHandling errorHandling;

		Writer output; /// nullptr means std::cerr; use getOutput() accessor

	public:
		FlagSet(std::string name, enum ErrorHandling errorHandling);

		FlagSet(const FlagSet &) = delete;
		FlagSet(FlagSet &&) noexcept = delete;
		FlagSet&operator=(const FlagSet &) = delete;
		FlagSet&operator=(FlagSet &&) noexcept = delete;

		~FlagSet() = default;

		/// @returns the destination for usage and error messages. std::cerr is returned if
		///          output was not set or was set to nullptr.
		[[nodiscard]] const Writer &getOutput() const;

		/// Sets the destination for usage and error messages.
		/// If output is nullptr, std::cerr is used.
		void setOutput(const Writer &w);

		/// @returns the name of the flag set.
		[[nodiscard]] const std::string &getName() const;

		/// @returns the error handling behavior of the flag set.
		[[nodiscard]] const enum ErrorHandling &getErrorHandling() const;

		/// Sets the usage function.
		void setUsage(UsageFunction &&f);

		/// Visits the flags in lexicographical order, calling @param f for each.
		/// It visits all flags, even those not set.
		void visitAll(std::function<void(const Flag &)> &&f) const;

		/// Prints, to @property output the default values of all defined command-line
		/// flags in the set.
		void printDefaults() const;

		/// Defines a bool flag with specified name, default value, and usage string.
		/// The argument p points to a bool variable in which to store the value of the flag.
		void boolVar(BoolVar &p, const std::string &name, bool value = false, const std::string &usage = "");

		/// Defines a bool flag with specified name, default value, and usage string.
		/// @return value is the address of a bool variable that stores the value of the flag.
		BoolVar newBool(const std::string &name, bool value = false, const std::string &usage = "");

		/// Defines a string flag with specified name, default value, and usage string.
		/// The argument p points to a string variable in which to store the value of the flag.
		void stringVar(StringVar &p, const std::string &name, const std::string &value = "",
					   const std::string &usage = "");

		/// Defines a string flag with specified name, default value, and usage string.
		/// @return value is the address of a string variable that stores the value of the flag.
		StringVar newString(const std::string &name, const std::string &value = "",
							const std::string &usage = "");

		/// Defines an int flag with specified name, default value, and usage string.
		/// The argument p points to an int variable in which to store the value of the flag.
		void intVar(IntVar &p, const std::string &name, int value = 0, const std::string &usage = "");

		/// Defines an int flag with specified name, default value, and usage string.
		/// @return value is the address of an int variable that stores the value of the flag.
		IntVar newInt(const std::string &name, int value = 0, const std::string &usage = "");

		/// Defines a flag with the specified name and usage string.
		/// Each time the flag is seen, f is called with the value of the flag.
		void func(const std::string &name, const std::string &usage,
				  std::function<void(const std::string &)> &&f);

		/// Defines a flag with the specified name and usage string.
		/// Each time the flag is seen, f is called with the int value of the flag.
		void func(const std::string &name, const std::string &usage,
				  std::function<void(const int &)> &&f);

		/// Parse parses flag definitions from the argument list, which should not
		/// include the command name. Must be called after all flags in the FlagSet
		/// are defined and before flags are accessed by the program.
		/// Throw "Help" exception if -help or -h were set but not defined.
		std::string parse(const std::vector<std::string> &arguments);

		/// Parse parses flag definitions from the argument list, which should not
		/// include the command name. Must be called after all flags in the FlagSet
		/// are defined and before flags are accessed by the program.
		/// Throw "Help" exception if -help or -h were set but not defined.
		std::string parse(int &argc, char **&argv);

		/// Reports whether parse has been called.
		[[nodiscard]] const bool &isParsed() const;

	private:
		/// Parses one flag. It reports whether a flag was seen.
		bool parseOne();

		/// Is the default function to print a usage message.
		void defaultUsage() const;

		/// Defines a flag with the specified name and usage string. The type and
		/// value of the flag are represented by the first argument, of type Value, which
		/// typically holds a user-defined implementation of Value. For instance, the
		/// caller could create a flag that turns a comma-separated string into a slice
		/// of strings by giving the slice the methods of Value; in particular, Set would
		/// decompose the comma-separated string into the slice.
		void var(const Value &value, const std::string &name, const std::string &usage);
	};
}

#endif //FLAG_FLAG_HPP
