//
// Created by egor9814 on 07 Aug 2021.
//

#ifndef FLAG_STRING_HPP
#define FLAG_STRING_HPP

#include <flag.hpp>

namespace flag {

	struct StringValue : public IValue {
		StringVar p;

		static Value create(const StringVar &p);

		explicit StringValue(StringVar p);

		[[nodiscard]] std::string toString() const override;

		void set(const std::string &s) override;

		[[nodiscard]] bool isString() const override;
	};

}

#endif //FLAG_STRING_HPP
