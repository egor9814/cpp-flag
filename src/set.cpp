//
// Created by egor9814 on 30 Jul 2021.
//

#include <flag.hpp>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include "bool.hpp"
#include "string.hpp"
#include "int.hpp"
#include "func.hpp"

namespace flag {

	FlagSet::FlagSet(std::string name, enum ErrorHandling errorHandling)
			: name(std::move(name)), errorHandling(errorHandling) {
		usage = [this] { defaultUsage(); };
	}

	static const FlagSet::Writer &cerr() {
		static FlagSet::Writer writer{&std::cerr, [](auto){}};
		return writer;
	}

	const FlagSet::Writer &FlagSet::getOutput() const {
		if (output)
			return output;
		return cerr();
	}

	void FlagSet::setOutput(const FlagSet::Writer &w) {
		output = w;
	}

	const std::string &FlagSet::getName() const {
		return name;
	}

	const FlagSet::ErrorHandling &FlagSet::getErrorHandling() const {
		return errorHandling;
	}

	void FlagSet::setUsage(UsageFunction &&f) {
		usage = std::move(f);
	}

	void FlagSet::visitAll(std::function<void(const Flag &)> &&f) const {
		if (f) {
			std::vector<FlagPtr> values;
			values.reserve(formal.size());
			for (const auto &it : formal) {
				values.push_back(it.second);
			}
			std::sort(values.begin(), values.end(), [](const auto &l, const auto &r) -> bool {
				return l->name < r->name;
			});
			for (const auto &it : values) {
				f(*it);
			}
		}
	}

	/// Extracts a back-quoted name from the usage
	/// string for a flag and returns it and the un-quoted usage.
	/// Given "a `name` to show" it returns ("name", "a name to show").
	/// If there are no back quotes, the name is an educated guess of the
	/// type of the flag's value, or the empty string if the flag is boolean.
	std::pair<std::string, std::string> unquoteUsage(const Flag &flag) {
		std::string name, usage;
		usage = flag.usage;
		for (auto i = usage.begin(), end = usage.end(); i != end; ++i) {
			if (*i == '`') {
				for (auto j = ++i; j != end; ++j) {
					if (*j == '`') {
						name = std::string(i, j);
						return std::make_pair(
								name,
								std::string(usage.begin(), i-1) + name + std::string(j+1, end)
						);
					}
				}
				break;
			}
		}
		if (!flag.value->isBool()) {
			using namespace std::string_view_literals;
			name = "value"sv;
		}
		return std::make_pair(name, usage);
	}

	void FlagSet::printDefaults() const {
		visitAll([this](const Flag &f) {
			std::stringstream s;
			s << "  -" << f.name;
			auto [name, usage] = unquoteUsage(f);
			// Boolean flags of one ASCII letter are so common we
			// treat them specially, putting their usage on the same line.
			if (!name.empty())
				s << " " << name;
			if (s.gcount() <= 4) { // space, space, '-', 'x'.
				s << '\t';
			} else {
				// Four spaces before the tab triggers good alignment
				// for both 4- and 8-space tab stops.
				s << "\n    \t";
			}

			for (size_t index = usage.find('\n'); index != std::string::npos; index = usage.find('\n', index)) {
				usage.replace(index, 1, "\n    \t");
				index += 6;
			}
			s << usage;

			if (!f.defaultValue.empty()) {
				s << " (default ";
				if (f.value->isString()) {
					s << '"' << f.defaultValue << '"';
				} else {
					s << f.defaultValue;
				}
				s << ")\n";
			}

			*getOutput() << s.view();
		});
	}

	void FlagSet::boolVar(BoolVar &p, const std::string &name, bool value, const std::string &usage) {
		*p = value;
		var(BoolValue::create(p), name, usage);
	}

	BoolVar FlagSet::newBool(const std::string &name, bool value, const std::string &usage) {
		auto p = std::make_shared<bool>();
		boolVar(p, name, value, usage);
		return p;
	}

	void FlagSet::stringVar(StringVar &p, const std::string &name, const std::string &value,
							const std::string &usage) {
		*p = value;
		var(StringValue::create(p), name, usage);
	}

	StringVar FlagSet::newString(const std::string &name, const std::string &value, const std::string &usage) {
		auto p = std::make_shared<std::string>();
		p->reserve(value.size());
		stringVar(p, name, value, usage);
		return p;
	}

	void FlagSet::intVar(IntVar &p, const std::string &name, int value, const std::string &usage) {
		*p = value;
		var(IntValue::create(p), name, usage);
	}

	IntVar FlagSet::newInt(const std::string &name, int value, const std::string &usage) {
		auto p = std::make_shared<int>();
		intVar(p, name, value, usage);
		return p;
	}

	void FlagSet::func(const std::string &name, const std::string &usage,
					   std::function<void(const std::string &)> &&f) {
		var(FuncValue<std::string>::create(std::move(f)), name, usage);
	}

	void FlagSet::func(const std::string &name, const std::string &usage,
					   std::function<void(const int &)> &&f) {
		var(FuncValue<int>::create(std::move(f), [](const std::string &s) -> int {
			char *endptr{nullptr};
			auto result = static_cast<int>(strtoll(s.data(), &endptr, 10) & 0xFFFFFFFF);
			if (s.data() == endptr) {
				throw std::runtime_error("not a integer: " + s);
			}
			return result;
		}), name, usage);
	}

	std::string FlagSet::parse(const std::vector<std::string> &arguments) {
		parsed = true;
		args = arguments;
		while (true) {
			try {
				auto seen = parseOne();
				if (seen)
					continue;
			} catch (const std::exception &err) {
				using namespace std::string_view_literals;
				switch (errorHandling) {
					case ErrorHandling::ContinueOnError:
						if (std::string_view(err.what()) != "::help"sv) {
							return err.what();
						}
					case ErrorHandling::ExitOnError:
						if (std::string_view(err.what()) == "::help"sv) {
							::exit(0);
						}
						::exit(2);
					case ErrorHandling::ThrowOnError:
						if (std::string_view(err.what()) != "::help"sv) {
							throw err;
						}
				}
			} catch (...) {
				throw;
			}
			break;
		}
		return std::string();
	}

	std::string FlagSet::parse(int &argc, char **&argv) {
		std::vector<std::string> args;
		args.reserve(argc-1);
		for (int i = 1; i < argc; i++)
			args.emplace_back(argv[i]);
		return parse(args);
	}

	const bool &FlagSet::isParsed() const {
		return parsed;
	}

	bool FlagSet::parseOne() {
		if (args.empty())
			return false;
		const auto &s = args.front();
		if (s.length() < 2 || s.front() != '-') {
			return false;
		}
		auto numMinuses = 1;
		if (s[1] == '-') {
			numMinuses++;
			if (s.length() == 2) {
				// "--" terminates the flags
				args.erase(args.begin());
				return false;
			}
		}
		auto name = s.substr(numMinuses);
		if (name.empty() || name.front() == '-' || name.front() == '=') {
			throw std::runtime_error("bad flag syntax: " + s);
		}

		// it's a flag. does it have an argument?
		args.erase(args.begin());
		auto hasValue = false;
		std::string value;
		for (auto it = name.begin(), end = name.end(); it != end; ++it) {
			if (*it == '=') {
				value = std::string(it+1, end);
				hasValue = true;
				name = std::string(name.begin(), it);
				break;
			}
		}
		auto flag = formal.find(name);
		if (flag == formal.end()) {
			if (name == "help" || name == "h") { // special case for nice help message.
				if (usage) usage();
				throw std::runtime_error("::help");
			}
			throw std::runtime_error("flag provided but not defined: -" + name);
		}

		if (auto fv = flag->second->value; fv->isBool()) {
			if (hasValue) {
				fv->set(value);
			} else {
				fv->set("true");
			}
		} else {
			if (!hasValue && !args.empty()) {
				hasValue = true;
				value = args.front();
				args.erase(args.begin());
			}
			if (!hasValue) {
				throw std::runtime_error("flag needs an argument: -" + name);
			}
			fv->set(value);
		}

		actual.insert(*flag);
		return true;
	}

	void FlagSet::defaultUsage() const {
		if (name.empty()) {
			*getOutput() << "Usage:\n";
		} else {
			*getOutput() << "Usage of " << name << ":\n";
		}
		printDefaults();
	}

	void FlagSet::var(const Value &value, const std::string &name, const std::string &usage) {
		if (formal.contains(name)) {
			std::stringstream s;
			if (!this->name.empty())
				s << this->name << " ";
			s << "flag redefined: " << name;
			*getOutput() << s.view() << "\n";
			throw std::runtime_error(s.str()); // Happens only if flags are declared with identical names
		}
		auto &flag = formal[name];
		flag = std::make_shared<Flag>(name, usage, value, value->toString());
	}
}