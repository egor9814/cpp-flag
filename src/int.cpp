//
// Created by egor9814 on 07 Aug 2021.
//

#include "int.hpp"
#include <stdexcept>

namespace flag {

	Value IntValue::create(const IntVar &p) {
		return std::make_shared<IntValue>(p);
	}

	IntValue::IntValue(IntVar p) : p(std::move(p)) {}

	std::string IntValue::toString() const {
		return std::to_string(*p);
	}

	void IntValue::set(const std::string &s) {
		char *endptr{nullptr};
		*p = static_cast<int>(strtoll(s.data(), &endptr, 10) & 0xFFFFFFFF);
		if (s.data() == endptr) {
			throw std::runtime_error("not a integer: " + s);
		}
	}

}
