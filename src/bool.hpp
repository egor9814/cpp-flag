//
// Created by egor9814 on 30 Jul 2021.
//

#ifndef FLAG_BOOL_HPP
#define FLAG_BOOL_HPP

#include <flag.hpp>

namespace flag {

	struct BoolValue : public IValue {
		BoolVar p;

		static Value create(const BoolVar &p);

		explicit BoolValue(BoolVar p);

		[[nodiscard]] std::string toString() const override;

		void set(const std::string &s) override;

		[[nodiscard]] bool isBool() const override;
	};

}

#endif //FLAG_BOOL_HPP
