//
// Created by egor9814 on 07 Aug 2021.
//

#include "string.hpp"

namespace flag {

	Value StringValue::create(const StringVar &p) {
		return std::make_shared<StringValue>(p);
	}

	StringValue::StringValue(StringVar p) : p(std::move(p)) {}

	std::string StringValue::toString() const {
		return *p;
	}

	void StringValue::set(const std::string &s) {
		*p = s;
	}

	bool StringValue::isString() const {
		return true;
	}

}
