//
// Created by egor9814 on 30 Jul 2021.
//

#include <flag.hpp>
#include <utility>

namespace flag {

	Flag::Flag(std::string name, std::string usage, Value value, std::string defaultValue)
			: name(std::move(name)),
			  usage(std::move(usage)),
			  value(std::move(value)),
			  defaultValue(std::move(defaultValue)) {}

}
