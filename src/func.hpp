//
// Created by egor9814 on 07 Aug 2021.
//

#ifndef FLAG_FUNC_HPP
#define FLAG_FUNC_HPP

#include <flag.hpp>

namespace flag {

	template <typename T>
	struct FuncValue : public IValue {
		using F = std::function<void(const T &)>;
		using C = std::function<T(const std::string &)>;

		F f;
		C c;

		static Value create(F &&f, C &&c) {
			return std::make_shared<FuncValue<T>>(std::move(f), std::move(c));
		}

		explicit FuncValue(F &&f, C &&c) : f(std::move(f)), c(std::move(c)) {}

		[[nodiscard]] std::string toString() const override {
			return "";
		}

		void set(const std::string &s) override {
			if (f && c)
				f(c(s));
		}
	};

	template <>
	struct FuncValue<std::string> : public IValue {
		using F = std::function<void(const std::string &)>;

		F f;

		static Value create(F &&f) {
			return std::make_shared<FuncValue<std::string>>(std::move(f));
		}

		explicit FuncValue(F &&f) : f(std::move(f)) {}

		[[nodiscard]] std::string toString() const override {
			return "";
		}

		void set(const std::string &s) override {
			if (f)
				f(s);
		}
	};

}

#endif //FLAG_FUNC_HPP
