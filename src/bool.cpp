//
// Created by egor9814 on 30 Jul 2021.
//

#include "bool.hpp"

namespace flag {

	Value BoolValue::create(const BoolVar &p) {
		return std::make_shared<BoolValue>(p);
	}

	BoolValue::BoolValue(BoolVar p) : p(std::move(p)) {}

	std::string BoolValue::toString() const {
		return *p ? "true" : "false";
	}

	void BoolValue::set(const std::string &s) {
		*p = "true" == s;
	}

	bool BoolValue::isBool() const {
		return true;
	}

}
