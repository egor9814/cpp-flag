//
// Created by egor9814 on 07 Aug 2021.
//

#ifndef FLAG_INT_HPP
#define FLAG_INT_HPP

#include <flag.hpp>

namespace flag {

	struct IntValue : public IValue {
		IntVar p;

		static Value create(const IntVar &p);

		explicit IntValue(IntVar p);

		[[nodiscard]] std::string toString() const override;

		void set(const std::string &s) override;
	};

}

#endif //FLAG_INT_HPP
