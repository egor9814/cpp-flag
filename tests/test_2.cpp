//
// Created by egor9814 on 02 Aug 2021.
//

#include <flag.hpp>
#include "common.hpp"

AutoTestUnit(Second)

AutoTestCase() {
	try {
		static const char *args[] = {
				"<SecondTest>",
				"-string1",
				"my_string",
				"-string2=not_my_string"
		};
		int argc = 4;
		auto argv = const_cast<char**>(args);

		flag::FlagSet flags{args[0], flag::FlagSet::ErrorHandling::ThrowOnError};
		auto string1 = flags.newString("string1", "default", "set any `string` with length > 0");
		auto string2 = flags.newString("string2", "default", "set any `string` with length > 0");
		auto string3 = flags.newString("string3", "default", "set any `string` with length > 0");
		if (auto err = flags.parse(argc, argv); !err.empty()) {
			TestRequireMessage(false, err);
		}

		TestCheck(*string1 == "my_string");
		TestCheck(*string2 == "not_my_string");
		TestCheck(*string3 == "default");
	} catch (const std::exception &err) {
		TestRequireMessage(false, err.what());
	} catch (...) {
		TestRequireMessage(false, "failed: unknown error");
	}
}

EndTestUnit(First)
