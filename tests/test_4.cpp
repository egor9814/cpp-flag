//
// Created by egor9814 on 02 Aug 2021.
//

#include <flag.hpp>
#include "common.hpp"
#include <vector>

AutoTestUnit(Fourth)

AutoTestCase() {
	try {
		static const char *args[] = {
				"<FourthTest>",
				"-i",
				"-1236586611",
				"-s",
				"str",
				"-i=124124",
				"-s=1245"
		};
		int argc = 7;
		auto argv = const_cast<char**>(args);

		std::vector<std::string> strings;
		std::vector<int> ints;

		flag::FlagSet flags{args[0], flag::FlagSet::ErrorHandling::ContinueOnError};
		flags.func("i", "add `integer` to list", [&ints](const int &i) {
			ints.push_back(i);
		});
		flags.func("s", "add `string` to list", [&strings](const std::string &s) {
			strings.push_back(s);
		});
		if (auto err = flags.parse(argc, argv); !err.empty()) {
			TestRequireMessage(false, err);
		}

		TestCheck(strings.size() == 2);
		TestCheck(strings[0] == "str");
		TestCheck(strings[1] == "1245");
		TestCheck(ints.size() == 2);
		TestCheck(ints[0] == -1236586611);
		TestCheck(ints[1] == 124124);
	} catch (const std::exception &err) {
		TestRequireMessage(false, err.what());
	} catch (...) {
		TestRequireMessage(false, "failed: unknown error");
	}
}

EndTestUnit(First)
