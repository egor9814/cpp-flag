//
// Created by egor9814 on 02 Aug 2021.
//

#include <flag.hpp>
#include "common.hpp"

AutoTestUnit(First)

AutoTestCase() {
	try {
		static const char *args[] = {
				"<FirstTest>",
				"-bool0",
				"-bool1=false",
				"-bool2"
		};
		int argc = 4;
		auto argv = const_cast<char**>(args);

		flag::FlagSet flags{args[0], flag::FlagSet::ErrorHandling::ThrowOnError};
		auto bool0 = flags.newBool("bool0", true, "set only `true|false` values");
		auto bool1 = flags.newBool("bool1", false, "set only `true|false` values");
		auto bool2 = flags.newBool("bool2", false, "set only `true|false` values");
		if (auto err = flags.parse(argc, argv); !err.empty()) {
			TestRequireMessage(false, err);
		}

		TestCheck(*bool0);
		TestCheck(!*bool1);
		TestCheck(*bool2);
	} catch (const std::exception &err) {
		TestRequireMessage(false, err.what());
	} catch (...) {
		TestRequireMessage(false, "failed: unknown error");
	}
}

EndTestUnit(First)
