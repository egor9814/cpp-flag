//
// Created by egor9814 on 02 Aug 2021.
//

#include <flag.hpp>
#include "common.hpp"

AutoTestUnit(Third)

AutoTestCase() {
	try {
		static const char *args[] = {
				"<ThirdTest>",
				"-int1",
				"-1236586611",
				"-int2=124124"
		};
		int argc = 4;
		auto argv = const_cast<char**>(args);

		flag::FlagSet flags{args[0], flag::FlagSet::ErrorHandling::ThrowOnError};
		auto int1 = flags.newInt("int1", 1, "set any `string` with length > 0");
		auto int2 = flags.newInt("int2", 2, "set any `string` with length > 0");
		auto int3 = flags.newInt("int3", 3, "set any `string` with length > 0");
		if (auto err = flags.parse(argc, argv); !err.empty()) {
			TestRequireMessage(false, err);
		}

		TestCheck(*int1 == -1236586611);
		TestCheck(*int2 == 124124);
		TestCheck(*int3 == 3);
	} catch (const std::exception &err) {
		TestRequireMessage(false, err.what());
	} catch (...) {
		TestRequireMessage(false, "failed: unknown error");
	}
}

EndTestUnit(First)
